package interfaces;

public interface IMovieRecyclerListener {
    void onLastScrollPosition();
}
