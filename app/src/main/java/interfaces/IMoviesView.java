package interfaces;

import joseprosello.mobiletest_privalia.models.PopularMoviesModel;
import joseprosello.mobiletest_privalia.models.SearchMoviesModel;

public interface IMoviesView {
    void processMovieResponse(PopularMoviesModel popularMoviesResponse);
    void processSearchResponse(SearchMoviesModel searchMoviesResponse);
    void onError();
}
