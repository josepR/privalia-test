package joseprosello.mobiletest_privalia.api;

import joseprosello.mobiletest_privalia.api.model.PopularMoviesResponse;
import joseprosello.mobiletest_privalia.api.model.SearchMoviesResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIService {
    @GET("/3/movie/popular")
    Call<PopularMoviesResponse> getPopularMovies(@Query("api_key") String apiKey,
                                                 @Query("page") int page);
    @GET("3/search/movie")

    Call<SearchMoviesResponse> callSearchMovies(@Query("api_key") String apiKey,
                                                @Query("page") int page,
                                                @Query("query") String query);
 }
