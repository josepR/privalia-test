package joseprosello.mobiletest_privalia.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import interfaces.IMovieRecyclerListener;
import joseprosello.mobiletest_privalia.R;
import joseprosello.mobiletest_privalia.models.LoadingViewModel;
import joseprosello.mobiletest_privalia.models.MovieViewModel;

import static joseprosello.mobiletest_privalia.api.RetrofitClient.BASE_IMAGE_URL;

public class MovieRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int VIEW_MOVIE = 0;
    public static final int VIEW_PROGRESS = 1;

    private List moviesList;
    private RequestManager mGlide;
    private int mTotalItemCount;
    private int mLastVisibleItem;
    private boolean mLoading;
    private IMovieRecyclerListener onMovieRecyclerListener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView title, year, overview;
        private ImageView image;

        public MyViewHolder(View view) {
            super(view);

            mGlide = Glide.with(view.getContext());

            title = (TextView) view.findViewById(R.id.movie_list_title);
            overview = (TextView) view.findViewById(R.id.movie_list_overview);
            year = (TextView) view.findViewById(R.id.movie_list_year);
            image = (ImageView) view.findViewById(R.id.movie_list_image);

        }
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);

            progressBar = view.findViewById(R.id.progressBar1);
        }
    }

    public MovieRecyclerAdapter(final List moviesList, RecyclerView recyclerView, IMovieRecyclerListener listener) {
        this.moviesList = moviesList;
        this.onMovieRecyclerListener = listener;

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                        .getLayoutManager();

                mTotalItemCount = linearLayoutManager.getItemCount();
                mLastVisibleItem = linearLayoutManager
                        .findLastVisibleItemPosition();
                if (!mLoading && mTotalItemCount <= (mLastVisibleItem + 1)) {
                    if (onMovieRecyclerListener != null) {
                        mLoading = true;
                        onMovieRecyclerListener.onLastScrollPosition();
                    }
                }
            }
        });
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        if (viewType == 0) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.movie_recycler_row, parent, false);
            viewHolder = new MyViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progressbar, parent, false);
            viewHolder = new LoadingViewHolder(itemView);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyViewHolder) {
            MovieViewModel movie = (MovieViewModel) moviesList.get(position);
            ((MyViewHolder) holder).title.setText(movie.getTitle());
            ((MyViewHolder) holder).overview.setText(movie.getOverview());
            Calendar calendar = Calendar.getInstance();
            DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            try {
                calendar.setTime(fmt.parse(movie.getYear()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            int year = calendar.get(Calendar.YEAR);
            ((MyViewHolder) holder).year.setText(String.valueOf(year));

            mGlide.load(BASE_IMAGE_URL + movie.getPicture())
                    .centerCrop()
                    .into(((MyViewHolder) holder).image);
        } else {
            ((LoadingViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        //By default return progress bar type
        int returnType = VIEW_PROGRESS;

        if (moviesList.get(position) instanceof MovieViewModel) {
            returnType = ((MovieViewModel) moviesList.get(position)).getID();
        } else if (moviesList.get(position) instanceof LoadingViewModel) {
            returnType = ((LoadingViewModel) moviesList.get(position)).getID();
        }
        return returnType;
    }

    public void setLoading(boolean mLoading) {
        this.mLoading = mLoading;
    }
}
