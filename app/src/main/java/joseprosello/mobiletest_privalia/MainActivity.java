package joseprosello.mobiletest_privalia;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import interfaces.IMovieRecyclerListener;
import interfaces.IMoviesView;
import joseprosello.mobiletest_privalia.adapter.MovieRecyclerAdapter;
import joseprosello.mobiletest_privalia.models.LoadingViewModel;
import joseprosello.mobiletest_privalia.models.MovieViewModel;
import joseprosello.mobiletest_privalia.models.PaginatorModel;
import joseprosello.mobiletest_privalia.models.PopularMoviesModel;
import joseprosello.mobiletest_privalia.models.SearchMoviesModel;
import presenter.MovieListPresenter;

public class MainActivity extends AppCompatActivity implements IMoviesView, IMovieRecyclerListener {

    List movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TextView errorMessage;
    private MovieRecyclerAdapter mAdapter;
    private MovieListPresenter mPresenter;
    private PaginatorModel mPaginator;
    private SearchView searchView;
    private boolean isSearch = false;
    private String mCurrentQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (mPresenter == null) {
            this.mPresenter = new MovieListPresenter();
        }
        if (savedInstanceState == null) {
            this.mPresenter.attachView(this);
        }

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        errorMessage = (TextView) findViewById(R.id.error_message);
        errorMessage.setText(R.string.no_results);

        createPaginator();
        fillData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                createPaginator();
                isSearch = true;
                mCurrentQuery = query;
                mPresenter.callSearchMovies(mPaginator, mCurrentQuery);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                createPaginator();
                isSearch = !query.isEmpty();
                movieList.clear();
                mCurrentQuery = query;
                if (isSearch) {
                    mPresenter.callSearchMovies(mPaginator, mCurrentQuery);

                } else {
                    mPresenter.getPopularMovies(mPaginator);
                }


                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }

    private void createPaginator() {
        mPaginator = new PaginatorModel();
        mPaginator.setPage(1);
    }

    private void fillData() {
        mPresenter.getPopularMovies(mPaginator);
    }

    private void createAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

    private void loadMovies(List<MovieViewModel> results) {
        if (mAdapter == null) {
            movieList = results;
            mAdapter = new MovieRecyclerAdapter(movieList, recyclerView, this);
            createAdapter();
        } else {
            movieList.addAll(results);
        }
    }

    @Override
    public void processMovieResponse(PopularMoviesModel popularMoviesModel) {
        showMovieList();
        mPaginator.setPage(popularMoviesModel.getPage());
        mPaginator.setTotalPages(popularMoviesModel.getTotalPages());


        loadMovies(popularMoviesModel.getResults());
        mAdapter.notifyDataSetChanged();
        mAdapter.setLoading(false);

        resetLoading();
        if (mPaginator.canLoadMore()) {
            showLoading();
        }
    }

    @Override
    public void processSearchResponse(SearchMoviesModel searchMoviesResponse) {
        showMovieList();
        mPaginator.setPage(searchMoviesResponse.getPage());
        mPaginator.setTotalPages(searchMoviesResponse.getTotalPages());


        loadMovies(searchMoviesResponse.getResults());
        mAdapter.notifyDataSetChanged();
        mAdapter.setLoading(false);

        resetLoading();
        if (mPaginator.canLoadMore()) {
            showLoading();
        }
    }

    public void showLoading() {
        movieList.add(new LoadingViewModel());
    }

    private void resetLoading() {
        for (int i = 0; i < movieList.size(); i++) {
            if (movieList.get(i) instanceof LoadingViewModel) {
                movieList.remove(i);
                mAdapter.notifyItemRemoved(i);
            }
        }
    }

    @Override
    public void onError() {
        //SHOW ERROR
        recyclerView.setVisibility(View.GONE);
        errorMessage.setVisibility(View.VISIBLE);
    }

    private void showMovieList(){
        recyclerView.setVisibility(View.VISIBLE);
        errorMessage.setVisibility(View.GONE);
    }

    @Override
    protected void onStart() {
        super.onStart();

        mPresenter.start();
    }

    @Override
    protected void onStop() {
        super.onStop();

        mPresenter.stop();
    }

    @Override
    public void onLastScrollPosition() {
        if (mPaginator.canLoadMore()) {
            mPaginator.setPage(mPaginator.getPage() + 1);
            if (isSearch) {
                mPresenter.callSearchMovies(mPaginator, mCurrentQuery);
            } else {
                mPresenter.getPopularMovies(mPaginator);
            }
        }
    }
}
