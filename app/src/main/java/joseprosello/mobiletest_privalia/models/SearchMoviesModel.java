package joseprosello.mobiletest_privalia.models;

import java.util.ArrayList;
import java.util.List;

public class SearchMoviesModel {
    private int page;
    private int totalPages;
    private int totalResults;
    private List<MovieViewModel> movieList = new ArrayList<>();

    public void setPage(int page){
        this.page = page;
    }

    public int getPage(){
        return page;
    }

    public void setTotalPages(int totalPages){
        this.totalPages = totalPages;
    }

    public int getTotalPages(){
        return totalPages;
    }

    public void setResults(List<MovieViewModel> results){
        this.movieList = results;
    }

    public List<MovieViewModel> getResults(){
        return movieList;
    }

    public void setTotalResults(int totalResults){
        this.totalResults = totalResults;
    }

    public int getTotalResults(){
        return totalResults;
    }

    @Override
    public String toString(){
        return
                "PopularMoviesResponse{" +
                        "page = '" + page + '\'' +
                        ",total_pages = '" + totalPages + '\'' +
                        ",movieList = '" + movieList + '\'' +
                        ",total_results = '" + totalResults + '\'' +
                        "}";
    }
}
