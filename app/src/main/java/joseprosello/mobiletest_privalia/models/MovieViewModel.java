package joseprosello.mobiletest_privalia.models;

import static joseprosello.mobiletest_privalia.adapter.MovieRecyclerAdapter.VIEW_MOVIE;

public class MovieViewModel {

    private String title;
    private String year;
    private String overview;
    private String picture;

    public int getID() {
        return VIEW_MOVIE;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
