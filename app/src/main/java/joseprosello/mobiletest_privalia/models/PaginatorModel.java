package joseprosello.mobiletest_privalia.models;

public class PaginatorModel {
    private int page;
    private int totalPages;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public boolean canLoadMore() {
        return page < totalPages;
    }
}
