package controller;

import org.greenrobot.eventbus.EventBus;

import joseprosello.mobiletest_privalia.api.APIService;
import joseprosello.mobiletest_privalia.api.RetrofitClient;
import joseprosello.mobiletest_privalia.api.model.PopularMoviesResponse;
import joseprosello.mobiletest_privalia.api.model.SearchMoviesResponse;
import joseprosello.mobiletest_privalia.models.PaginatorModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static joseprosello.mobiletest_privalia.api.RetrofitClient.API_KEY;

public class MovieListController {
    private static MovieListController instance;

    public static MovieListController getInstance() {
        if (instance == null) {
            instance = new MovieListController();
        }
        return instance;
    }

    public void getPopularMovies(PaginatorModel paginator) {
        RetrofitClient.getClient().create(APIService.class)
                .getPopularMovies(API_KEY, paginator.getPage()).enqueue(new Callback<PopularMoviesResponse>() {
            @Override
            public void onResponse(Call<PopularMoviesResponse> call, Response<PopularMoviesResponse> response) {
                EventBus.getDefault().postSticky(response.body());
            }

            @Override
            public void onFailure(Call<PopularMoviesResponse> call, Throwable t) {
                EventBus.getDefault().postSticky(t);
            }
        });
    }

    public void callSearchMovies(PaginatorModel paginator, String query) {
        RetrofitClient.getClient().create(APIService.class)
                .callSearchMovies(API_KEY, paginator.getPage(), query).enqueue(new Callback<SearchMoviesResponse>() {
            @Override
            public void onResponse(Call<SearchMoviesResponse> call, Response<SearchMoviesResponse> response) {
                EventBus.getDefault().postSticky(response.body());
            }

            @Override
            public void onFailure(Call<SearchMoviesResponse> call, Throwable t) {
                EventBus.getDefault().postSticky(t);
            }
        });
    }
}
