package mappers;

import joseprosello.mobiletest_privalia.api.model.ResultsItem;
import joseprosello.mobiletest_privalia.api.model.SearchMoviesResponse;
import joseprosello.mobiletest_privalia.models.MovieViewModel;
import joseprosello.mobiletest_privalia.models.SearchMoviesModel;

public class SearchMovieMapper {
    public static SearchMoviesModel fromApi(SearchMoviesResponse response) {
        SearchMoviesModel moviesModel = new SearchMoviesModel();
        moviesModel.setPage(response.getPage());
        moviesModel.setTotalPages(response.getTotalPages());
        moviesModel.setTotalResults(response.getTotalResults());

        for (ResultsItem result : response.getResults()) {
            MovieViewModel item = new MovieViewModel();
            item.setYear(result.getReleaseDate());
            item.setOverview(result.getOverview());
            item.setTitle(result.getTitle());
            item.setPicture(result.getPosterPath());
            moviesModel.getResults().add(item);
        }
        return moviesModel;
    }
}
