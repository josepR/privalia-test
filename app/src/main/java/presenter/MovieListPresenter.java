package presenter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import controller.MovieListController;
import interfaces.IMoviesView;
import joseprosello.mobiletest_privalia.api.model.PopularMoviesResponse;
import joseprosello.mobiletest_privalia.api.model.SearchMoviesResponse;
import joseprosello.mobiletest_privalia.models.PaginatorModel;
import mappers.PopularMovieMapper;
import mappers.SearchMovieMapper;

public class MovieListPresenter extends BasePresenter<IMoviesView> {
    public void getPopularMovies(PaginatorModel mPaginator) {
        MovieListController.getInstance().getPopularMovies(mPaginator);

    }
    public void callSearchMovies(PaginatorModel mPaginator, String query) {
        MovieListController.getInstance().callSearchMovies(mPaginator, query);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPopularMoviesSuccess(PopularMoviesResponse response) {
        EventBus.getDefault().removeStickyEvent(PopularMoviesResponse.class);
        if (!response.getResults().isEmpty()) {
            this.getView().processMovieResponse(PopularMovieMapper.fromApi(response));
        } else {
            this.getView().onError();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSearchMoviesSuccess(SearchMoviesResponse response) {
        EventBus.getDefault().removeStickyEvent(SearchMoviesResponse.class);
        if (!response.getResults().isEmpty()) {
            this.getView().processSearchResponse(SearchMovieMapper.fromApi(response));
        } else {
            this.getView().onError();
        }
    }
}
