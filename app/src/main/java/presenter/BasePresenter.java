package presenter;

import org.greenrobot.eventbus.EventBus;

public class BasePresenter<T> {
    protected T mView;
    protected EventBus mBus;
    protected boolean mRegistered;

    public T getView() {
        return this.mView;
    }

    /**
     * Called when the presenter is created
     *
     * @param mView current view
     */
    public void attachView(T mView) {
        this.mView = mView;
    }

    /**
     * Called when the presenter is initialized
     */
    public void start() {
        if (!mRegistered) {
            //Register to the bus
            mBus = EventBus.getDefault();
            mBus.register(this);
            mRegistered = true;
        }
    }

    /**
     * Called when presenter is destroyed
     */
    public void stop() {
        if (mRegistered) {
            mBus.unregister(this);
            mRegistered = false;
        }
    }
}
